let data = [
    {
        name: "James",
        age: "30"
    },
    {
        name: "Sarah",
        age: "32"
    },
    {
        name: "Alex",
        age: "35"
    },
    {
        name: "Jordi",
        age: "28"
    },
    {
        name: "Mark",
        age: "37"
    },
    {
        name: "Julia",
        age: "27"
    },
];

const info = document.querySelector("#info");

let details = data.map(function(item){
    return '<div>' + item.name + ' ' +'is ' + item.age +' years old' + '</div>'
});

info.innerHTML = details.join('\n');