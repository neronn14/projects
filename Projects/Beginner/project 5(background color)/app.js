const button = document.querySelector("button")
const body = document.querySelector("body")
const color = ["blue", "red", "gray", "green", "#477774"]

body.style.backgroundColor = "red"

body.addEventListener("click", changeB)

function changeB() {
   const colorIndex = parseInt(Math.random() * color.length)
   body.style.backgroundColor = color[colorIndex]
}



