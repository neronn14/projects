const images = [
    "img-1",
    "img-2",
    "img-3",
];

let counter = 0

const buttons = document.querySelectorAll(".btn")
const imgDiv = document.querySelector(".img-container")

buttons.forEach(function(button){
    button.addEventListener("click", function(e){
        if(button.classList.contains("icon-arrow-left2")){
            counter--
            if(counter < 0){
                counter = images.length - 1
            }
            imgDiv.style.background = `url('img/${images[counter]}.jpg')`
        }
        if(button.classList.contains("icon-arrow-right2")){
            counter++
            if(counter > images.length - 1){
                counter = 0
            }
            imgDiv.style.background = `url('img/${images[counter]}.jpg')`
        }
    })
})